import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView } from 'react-native'

export default class Table extends Component {
    render() {
        return (
            <ScrollView style={{ flex: 1, flexDirection: 'colum' }}>
                
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text style={styles.tt}>วันที่</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.tt}>ข้อมูล</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.tt}>พลังงาน</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.tt}>แคลอรี่</Text>
                    </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text>14/5/2563</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>ข้าวมันไก่</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>120</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>120</Text>
                    </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text>14/5/2563</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>ข้าวมันไก่</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>120</Text>
                        
                    </View>
                    <View style={styles.row}>
                        <Text>240</Text>
                    </View>
                </View>


                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text>14/5/2563</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>วิ่ง</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>100</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>140</Text>
                    </View>
                </View>
                
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text>14/5/2563</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>ข้าวมันไก่</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>120</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>260</Text>
                    </View>
                </View>
                
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text>14/5/2563</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>วิ่ง</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>100</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>160</Text>
                    </View>
                </View>
                
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'powderblue' }}>
                    <View style={styles.row}>
                        <Text>14/5/2563</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>วิ่ง</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>100</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>60</Text>
                    </View>
                </View>



            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        backgroundColor: 'powderblue',
        borderWidth: 0.5,

    },
    tt : {
        alignSelf: 'center' 
    }

})
