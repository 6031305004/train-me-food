import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'


export default class home extends Component {
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ width: 150, height: 150, backgroundColor: 'powderblue', marginTop: 20, alignSelf: 'center' }}>

                </View>
                <Text style={{ alignSelf: 'center' }}>
                    คุณ มาลิกา ซ่อนมะลิ
                    </Text>

                <View style={{ flex: 1, width: 300, backgroundColor: 'skyblue', marginTop: 20, alignSelf: 'center', flexDirection: 'column' }}>

                    <View style={{ backgroundColor: 'red' }}>
                        <Text>
                            รูปแบบ ลดน้ำหนัก
                        </Text>
                    </View>
                    <View style={{ backgroundColor: 'yellow' }}>
                        <Text>
                            น้ำหนัก : 60
                            {"\n"}
                            ส่วนสูง : 165
                        </Text>

                    </View>
                    <View style={{ backgroundColor: 'green' }}>
                        <Text>
                            เป้าหมาย 60 -> 55
                        </Text>
                    </View>
                    <View style={{ backgroundColor: 'pink' }}>
                        <Text>
                            ตังเป้าหมายไว้ : 50 วัน{"\n"}ได้รับแคลอรี่วันนี้ : 0{"\n"}การเผาผลานวันนี้ : 0
                        </Text>
                    </View>
                 
                    <View style={{ backgroundColor: '#FDBF' }}>
                        <Text>
                            กราฟ
                        </Text>
                    </View>


                </View>

                <Button
                    title='เลือก รูปแบบการบริโภค'
                    onPress={() => this.props.navigation.push('Choice')}
                ></Button>

            </View>
        )
    }
}

const styles = StyleSheet.create({})
