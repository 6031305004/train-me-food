import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Button } from 'react-native'


export default class choice extends Component {
    render() {
        return (

            <View>

                <Text style={{alignSelf: 'center' , marginTop : 50}}> เลือกรูปแบบที่ต้องการ</Text>

                <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center', marginTop : 50 }}>
                    

                    <TouchableOpacity >
                        <View style={styles.pack}>
                            <Text>ลดน้ำหนัก</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.pack}>
                            <Text>รักษาน้ำหนัก</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.pack}>
                            <Text>เพิ่มน้ำหนัก</Text>
                        </View>
                    </TouchableOpacity>


                </View>
                

            </View>

        )
    }
}

const styles = StyleSheet.create({
    pack: {
  
        backgroundColor: 'skyblue',
        margin: 10,
        alignSelf: 'center',
        width: 100,
        height: 450

    }

})
