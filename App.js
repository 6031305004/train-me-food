import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';


import 'react-native-gesture-handler';
import home from './scr/home';
import food from './scr/food';
import exersice from './scr/exersice';
import history from './scr/history';
import choice from './scr/choice';



const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const FoodStack = createStackNavigator();
const ExerStack = createStackNavigator();
const HistoryStack = createStackNavigator();


const HomeStackSreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={home} />
    <HomeStack.Screen name="Choice" component={choice} />
  </HomeStack.Navigator>
)
const FoodStackSreen = () => (
  <FoodStack.Navigator>
    <FoodStack.Screen name="Food" component={food} />
  </FoodStack.Navigator>
)
const ExerStackSreen = () => (
  <ExerStack.Navigator>
    <ExerStack.Screen name="Exersice" component={exersice} />
  </ExerStack.Navigator>
)
const HistoryStackSreen = () => (
  <HistoryStack.Navigator>
    <HistoryStack.Screen name="History" component={history} />
  </HistoryStack.Navigator>
)

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Profile" component={HomeStackSreen} />
        <Tab.Screen name="อาหาร" component={FoodStackSreen} />
        <Tab.Screen name="ออกกำลัง" component={ExerStackSreen} />
        <Tab.Screen name="ข้อมูล" component={HistoryStackSreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}